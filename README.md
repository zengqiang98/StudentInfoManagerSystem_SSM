# StudentInfoManagerSystem_SSM

#### 介绍
##### 学生信息管理系统基于SSM框架实现
##### 前端页面使用easyui框架模板搭建
##### 使用mysql数据库存储数据

#### 涉及知识点
##### SSM连接数据库
##### 以及对数据库的增删改查
##### 验证码
##### 分页显示
##### 以及SSM框架环境的搭建以及配置
##### SSM注解的使用

#### 项目演示

![输入图片说明](https://images.gitee.com/uploads/images/2019/1007/164647_a39bc185_5269223.png "屏幕截图.png")]
![输入图片说明](https://images.gitee.com/uploads/images/2019/1007/164746_857ef513_5269223.png "屏幕截图.png")]

![输入图片说明](https://images.gitee.com/uploads/images/2019/1007/164737_7ac71e22_5269223.png "屏幕截图.png")]
![输入图片说明](https://images.gitee.com/uploads/images/2019/1007/164730_376dc03e_5269223.png "屏幕截图.png")]
![输入图片说明](https://images.gitee.com/uploads/images/2019/1007/164720_439620bd_5269223.png "屏幕截图.png")]
![输入图片说明](https://images.gitee.com/uploads/images/2019/1007/164712_04710530_5269223.png "屏幕截图.png")]
